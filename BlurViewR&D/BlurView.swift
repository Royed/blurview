//
//  BlurView.swift
//  BlurViewR&D
//
//  Created by Roy on 22/11/16.
//  Copyright © 2016 InApp. All rights reserved.
//

import UIKit

@IBDesignable class BlurView: UIVisualEffectView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var shouldAnimate : Bool = true
    var animationDuration : Float = 0.5
    var overlayMargins:UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
    override var isHidden: Bool{
        get {
            return super.isHidden;
        }
        set {
            showView(show: !newValue, animated: shouldAnimate)
        }
    }
    
    class var sharedObj : BlurView {
        struct Singleton {
            // lazily initiated, thread-safe from "let"
            static let instance = BlurView()
        }
        return Singleton.instance
    }
    
    private var _blurEffectStyle : UIBlurEffectStyle = UIBlurEffectStyle.dark
    private var _effect : UIVisualEffect? = nil
    private let maxAlpha : CGFloat = 1.0
    private let minAlpha : CGFloat = 0.0
    
    private var touchedCompletionHandler : (()->())? = nil

    func add(overlayView:UIView) {
        self.contentView.addSubview(overlayView)
        setupConstraints(aView: overlayView, secondView: self.contentView, margins: overlayMargins)
    }
    
    
    @IBInspectable var blurEffectStyle_int : UInt { //UIBlurEffectStyle calculated from UInt because IBInspectable cannot be Enum
        get {
            return UInt(_blurEffectStyle.rawValue)
        }
        set {
            _effect = nil
            if let bEffectStyle = UIBlurEffectStyle(rawValue: Int(newValue)) {
                _blurEffectStyle = bEffectStyle
            } else {
                _blurEffectStyle = .dark
            }
            setUpView()
        }
    }
    
    //MARK: Class Methods
    class func showBlurView(withSubView:UIView?, animated:Bool = true, blurStyle:UIBlurEffectStyle = UIBlurEffectStyle.light, touchedView:(()->())? = nil) {
        let window = UIApplication.shared.keyWindow!
        sharedObj.frame = window.bounds
        sharedObj.blurEffectStyle_int = UInt(blurStyle.rawValue)
        if let _ = withSubView {
            sharedObj.add(overlayView: withSubView!)
        }
        sharedObj.touchedCompletionHandler = touchedView
        sharedObj.shouldAnimate = animated
        window.addSubview(sharedObj)
        sharedObj.setupConstraints(aView: sharedObj, secondView: window)
        sharedObj.isHidden = false
    }
    
    class func dismissBlurView() {
        sharedObj.showView(show: false) {
            sharedObj.removeFromSuperview()
        }
    }
    
    //MARK:Lifecycle Methods
    override init(effect: UIVisualEffect?) {
        if effect == UIBlurEffect() {
            super.init(effect: effect)
        } else{
            super.init(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
        }
        setUpView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    //MARK: Touches
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touchComplettion = touchedCompletionHandler {
            touchComplettion()
        }
    }
    
    //MARK: Custom Methods
    private func setUpView() {
        if let _ = _effect {
         self.effect = _effect!
        } else {
            _effect = UIBlurEffect(style: _blurEffectStyle)
            self.effect = _effect
        }
    }
    
    func showView(show:Bool, animated:Bool? = nil, completedAnim:(()->())? = nil) {
        let finalAlpha : CGFloat  = show ? maxAlpha : minAlpha
        let initialAlpha : CGFloat = show ? minAlpha : maxAlpha
        var shdAnimate = shouldAnimate
        if let _ = animated {
            shdAnimate = animated!
        }
        let duration = shdAnimate ? animationDuration : 0.0
        if isHidden == show { //Need to animate only if there is a transition of state (hidden or visible)
            super.isHidden = false
            self.alpha = initialAlpha
            self.contentView.alpha = initialAlpha
            UIView.animate(withDuration: TimeInterval(duration), animations: {
                self.alpha = finalAlpha
                self.contentView.alpha = finalAlpha
            }) { (completed) in
                super.isHidden = !show
                if let _ = completedAnim {
                    completedAnim!()
                }
            }
        }
    }
    
    private func setupConstraints(aView:UIView, secondView:UIView, margins:UIEdgeInsets = UIEdgeInsets.zero) {
        
        aView.translatesAutoresizingMaskIntoConstraints = false
        aView.leftAnchor.constraint(equalTo: secondView.leftAnchor, constant: margins.left).isActive = true
        aView.rightAnchor.constraint(equalTo: secondView.rightAnchor, constant: margins.right).isActive = true
        aView.bottomAnchor.constraint(equalTo: secondView.bottomAnchor, constant: margins.bottom).isActive = true
        aView.topAnchor.constraint(equalTo: secondView.topAnchor, constant: margins.top).isActive = true
        self.layoutSubviews()
    }
}
