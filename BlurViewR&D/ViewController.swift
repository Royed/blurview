//
//  ViewController.swift
//  BlurViewR&D
//
//  Created by Roy on 17/11/16.
//  Copyright © 2016 InApp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var fogView: BlurView!
    @IBOutlet weak var darkBlurView: BlurView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        darkBlurView.shouldAnimate = false
        let sView = PopView()
        sView.bounds = CGRect(x: 0, y: 0, width: 100, height: 100)
        fogView.add(overlayView: sView)
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func showHideBtnAction(_ sender: AnyObject) {
        fogView.isHidden = !fogView.isHidden
        darkBlurView.isHidden = !darkBlurView.isHidden
        BlurView.showBlurView(withSubView: PopView()) {
            print("Touched View")
            BlurView.dismissBlurView()
        }
    }
}

