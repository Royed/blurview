//
//  PopView.swift
//  BlurViewR&D
//
//  Created by Roy on 22/11/16.
//  Copyright © 2016 InApp. All rights reserved.
//

import UIKit

class PopView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    func loadFromNib() ->UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "PopView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViewForXib()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViewForXib()
    }
    
    func setupViewForXib(){
        let popView = loadFromNib()
        popView.frame = bounds
        popView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        self.addSubview(popView)
    }
}
